import { Component, OnInit } from '@angular/core';
import {CRUDService} from "../service/crud.service";
import {ActivatedRoute} from "@angular/router";
import {Employe} from "../models/employe";

@Component({
  selector: 'app-product-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  // @ts-ignore
  details: Employe;

  constructor(
    private crudService: CRUDService,
    private activatedRoute: ActivatedRoute
              ) { }

  ngOnInit(): void {
    let id ='';
    if(this.activatedRoute.snapshot.params['id']) {
      id = this.activatedRoute.snapshot.params['id'];
      if(id !='')
      {
        this.loadDetails(id);
      }
    }
  }

  loadDetails(id: any)
  {
    this.crudService.loadInfo(id).subscribe(res=>{
      this.details=res;
    })
  }
}
