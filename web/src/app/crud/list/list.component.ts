import {Component, OnInit} from '@angular/core';
import {CRUDService} from "../service/crud.service";
import {Router} from "@angular/router";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-product-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  columnDefs=[
    {
      field: 'name',
      headerName: 'Name',
      sortable: true
    },
    { field: 'gender' },
    { field: 'departement'},
    {
      field: '',
      headerName: 'Actions',
      width: 250,
      cellRenderer: this.actionRender.bind(this)
    }
  ];

  rowData: any = [];

  productList: any =[];
  productSubscribe:any;

  gridOptions = {
    rowHeight: 50
  }

  constructor(
    private crudService: CRUDService,
    private router: Router
              ) {

  }

  ngOnInit(): void {
    this.getEmployeList();
  }

  getEmployeList()
  {
    this.productSubscribe = this.crudService.loadProducts().subscribe(res=>{
      this.productList = res;
      this.rowData = res;
    })
  }

  actionRender(params: any)
  {
    let div = document.createElement('div');
    div.innerHTML = `
      <button type="button" class="btn btn-success"> View</button>\n
      <button type="button" class="btn btn-warning">Edit</button>\n
      <button type="button" class="btn btn-danger">Delete</button>\n
    `;

    let viewButton = div.querySelector('.btn-success');
    // @ts-ignore
    viewButton.addEventListener('click', ()=>
    {
      this.viewDetails(params);
    });


    let editButton = div.querySelector('.btn-warning');
    // @ts-ignore
    editButton.addEventListener('click', ()=>
    {
      this.editDetails(params);
    });


    let deleteButton = div.querySelector('.btn-danger');
    // @ts-ignore
    deleteButton.addEventListener('click', ()=>
    {
      this.deleteEmploye(params);
    });
    return div;
  }

  viewDetails(params: any)
  {
    this.router.navigate(['/api/view-details/' + params.data.id]).then(()=>{})
  }

  editDetails(params: any)
  {
    this.router.navigate(['/api/update/'+ params.data.id]).then(()=>{})
  }

  deleteEmploye(params: any)
  {
    const that = this;
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result: any) => {
      if (result.isConfirmed) {
        that.crudService.deleteEmploye(params.data.id).subscribe(res=>{
          console.log("res",res.result);
          if(res.result === 'success')
          {
            this.getEmployeList();
            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            ).then(() => {})
          }
          else
          {
            Swal.fire(
              'Not Deleted!',
              'Your file has not been deleted.',
              'error'
            ).then(() => {})
          }
        });
      }
    })
  }
}
