import { Component, OnInit } from '@angular/core';
import {CRUDService} from "../service/crud.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-product-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  // @ts-ignore
  employeForm: FormGroup;
  id: any;
  buttonText = 'Create';

  constructor(
    private  crudService : CRUDService,
    private formBuilder: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.createForm();
    let id ='';
    if(this.activatedRoute.snapshot.params['id']) {
      id = this.activatedRoute.snapshot.params['id'];
      if(id !='')
      {
        this.loadDetails(id);
      }
    }
  }

  createForm()
  {
    this.employeForm = this.formBuilder.group({
      'name': ['', Validators.compose([Validators.required, Validators.minLength(5)])],
      'gender': [''],
      'departement': ['']
    });
  }

  create(values: any)
  {
    let dataForm= {
      'name': values.name,
      'gender': values.gender,
      'departement': values.departement,
      'date': new Date(),
      'id':null
    }


    if(this.id)
    {
      dataForm.id= this.id;
      this.crudService.update(dataForm).subscribe(res=>{
        if(parseFloat(res.id)>0)
        {
          this.router.navigate(['/api/list']).then(()=>{});
        }
      });
    }
    else
    {
      this.crudService.create(dataForm).subscribe(res=>{
        if(parseFloat(res.id)>0)
        {
          this.router.navigate(['/api/list']).then(()=>{});
        }
      });
    }
  }

  loadDetails(id: any)
  {
    this.buttonText= 'Update';
    this.crudService.loadInfo(id).subscribe(res=>{
      this.employeForm.controls['name'].setValue(res.name);
      this.employeForm.controls['gender'].setValue(res.gender);
      this.employeForm.controls['departement'].setValue(res.departement);
      this.id=res.id;
    })
  }
}
