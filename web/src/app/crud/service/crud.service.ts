import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {map} from "rxjs/operators";
import {Observable} from "rxjs";
import {HttpResponse} from "../models/http-response";
import {Employe} from "../models/employe";

@Injectable({
  providedIn: 'root'
})
export class CRUDService {

  constructor(private httpClient: HttpClient) { }

  loadProducts()
  {
    const url= environment.API_EndPoint +'employees';
    return this.httpClient.get(url).pipe(map(data=>data));
  }

  create(data: any): Observable<HttpResponse>
  {
    const url= environment.API_EndPoint +'employees';
    return this.httpClient.post<HttpResponse>(url, data).pipe(map(data=>data));
  }

  loadInfo(id: any): Observable<Employe>
  {
    const url= environment.API_EndPoint +'employees/'+id;
    return this.httpClient.get<Employe>(url).pipe(map(data=>data));
  }

  update(data: any): Observable<HttpResponse>
  {
    const url= environment.API_EndPoint +'employees';
    return this.httpClient.put<HttpResponse>(url, data).pipe(map(data=>data));
  }

  deleteEmploye(id: any): Observable<HttpResponse>
  {
    const url = environment.API_EndPoint + 'employees/' + id;
    return this.httpClient.delete<HttpResponse>(url).pipe(map(data=>data));
  }
}
