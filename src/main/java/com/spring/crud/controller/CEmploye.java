package com.spring.crud.controller;

import com.spring.crud.model.MEmploye;
import com.spring.crud.service.SEmploye;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class CEmploye {
    private final SEmploye employe;

    public CEmploye(SEmploye employe) {
        this.employe = employe;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/employees")
    public List<MEmploye> get()
    {
        return employe.get();
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/employees")
    public MEmploye save(@RequestBody MEmploye _employe)
    {
        this.employe.save(_employe);
        return _employe;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/employees/{id}")
    public MEmploye get(@PathVariable Long id)
    {
        if(this.employe.get(id) != null) {
            return this.employe.get(id);
        }
        else
        {
            throw new RuntimeException("The Employee with id=" + id +" is not found");
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping("/employees/{id}")
    public Map <String, String> delete(@PathVariable Long id)
    {
        Map <String, String> data;
        data = new HashMap<>();
        data.put("result", "success");
        this.employe.delete(id);
        return data;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping("/employees")
    public MEmploye update(@RequestBody MEmploye _employe)
    {
        this.employe.save(_employe);
        return _employe;
    }
}
