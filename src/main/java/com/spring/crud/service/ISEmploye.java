package com.spring.crud.service;

import com.spring.crud.model.MEmploye;

import java.util.List;

public interface ISEmploye {
    List<MEmploye> get();

    MEmploye get(Long id);

    void save(MEmploye employe);

    void delete(Long id);
}
