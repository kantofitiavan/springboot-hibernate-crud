package com.spring.crud.service;

import com.spring.crud.dao.DEmploye;
import com.spring.crud.model.MEmploye;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SEmploye implements ISEmploye{
    private final DEmploye employe;

    public SEmploye(DEmploye employe) {
        this.employe = employe;
    }

    @Transactional
    @Override
    public List<MEmploye> get()
    {
        return this.employe.get();
    }

    @Transactional
    @Override
    public MEmploye get(Long id)
    {
        return this.employe.get(id);
    }

    @Transactional
    @Override
    public void save(MEmploye employe) {
        this.employe.save(employe);
    }

    @Transactional
    @Override
    public void delete(Long id) {
        this.employe.delete(id);
    }
}
