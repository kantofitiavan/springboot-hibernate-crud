package com.spring.crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudApplication {

	public static void main(String[] args)
	{
		System.out.println("--------------[BEGIN]--------------");
		SpringApplication.run(CrudApplication.class, args);
		System.out.println("---------------[END]-------------");
	}

}
