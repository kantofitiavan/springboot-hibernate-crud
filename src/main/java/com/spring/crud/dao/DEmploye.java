package com.spring.crud.dao;

import com.spring.crud.model.MEmploye;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class DEmploye implements IDEmploye{
    private final EntityManager entityManager;

    public DEmploye(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<MEmploye> get() {
        Session currentSession =  this.entityManager.unwrap(Session.class);
        Query<MEmploye> query =  currentSession.createQuery("from MEmploye", MEmploye.class);
        return query.getResultList();
    }

    @Override
    public MEmploye get(Long id) {
        Session currentSession =  this.entityManager.unwrap(Session.class);
        return currentSession.get(MEmploye.class, id);
    }

    @Override
    public void save(MEmploye employe) {
        Session currentSession =  this.entityManager.unwrap(Session.class);
        currentSession.saveOrUpdate(employe);
    }

    @Override
    public void delete(Long id) {
        Session currentSession =  this.entityManager.unwrap(Session.class);
        currentSession.delete(currentSession.get(MEmploye.class, id));
    }
}
