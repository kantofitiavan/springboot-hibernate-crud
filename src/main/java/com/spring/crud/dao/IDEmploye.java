package com.spring.crud.dao;

import com.spring.crud.model.MEmploye;

import java.util.List;

public interface IDEmploye {
    List<MEmploye> get();

    MEmploye get(Long id);

    void save(MEmploye employe);

    void delete(Long id);
}
